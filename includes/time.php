<?php
$tempo = <<<EOD
<div><html>
<head>
<link rel="stylesheet" type="text/css" href="./style/blue_style.css">
<script>
function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('clock').innerHTML = h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>
</head>

<body onload="startTime()">
Ora del Server<font size='4' face='comic sans ms'>:</font>
<div id="clock"></div>

</body>
</html>
</div>
EOD;
?>